import com.google.common.base.Splitter;

import java.util.Iterator;

/**
 * @author 方俊
 * @date 2020-11-04 14:17
 */
public class User {
    public static void main(String[] args) {
        String line = "  This  is  a   字符串  ";
        Iterable<String> words = Splitter.on(' ').trimResults().omitEmptyStrings().split(line);
        for (String word : words) {
            System.out.println(word);
        }
    }
}
